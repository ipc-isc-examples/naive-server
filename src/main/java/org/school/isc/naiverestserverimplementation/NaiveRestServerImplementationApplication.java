package org.school.isc.naiverestserverimplementation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NaiveRestServerImplementationApplication {

    public static void main(String[] args) {
        SpringApplication.run(NaiveRestServerImplementationApplication.class, args);
    }

}
