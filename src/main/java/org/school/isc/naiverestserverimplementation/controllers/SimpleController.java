package org.school.isc.naiverestserverimplementation.controllers;

import org.school.isc.api.RootApi;
import org.school.isc.model.Simple;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController implements RootApi {

    @Override
    public ResponseEntity<Simple> getSimplePerson(String name, String surname) {
        final Simple person = new Simple();
        person.setName(name);
        person.setSurname(surname);
        person.setTag("teacher");
        return ResponseEntity.ok(person);
    }
}
